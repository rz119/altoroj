package com.ibm.security.appscan.altoromutual.util;

import com.alibaba.fastjson.JSONObject;

import javax.json.stream.JsonParser;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class YahooFinanceUtil {

    public static Date getDate(String time){
        /**
         * @param: time in "YYYY-mm-dd" format
         * @return: Date formatted date
         */
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = simpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static long dateToTimestamp(){
        /**
         * @return: UNIX timestamp
         */
        Date dt = new Date();
        return dt.getTime();
    }

    public static long dateToTimestamp(Date date){
        /**
         * @param: date
         * @return: UNIX timestamp
         */
        return date.getTime();
    }

    public static Date timestampToDate(String timestamp){
        /**
         * @param: UNIX timestamp
         * @return: date
         */
        long timestampNo = Long.parseLong(timestamp);
        timestampNo *= 1000L;
        Date myDate = new Date();
        myDate.setTime(timestampNo);
        return myDate;
    }

    public static String fetchData(String uri, String apiKey) throws IOException,
            InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(uri))
                .header("x-api-key", apiKey)
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = HttpClient.newHttpClient()
                .send(request, HttpResponse.BodyHandlers.ofString());
        return response.body();
    }

    public static double fetchLiveBuy(String uri, String apiKey) throws IOException,
            InterruptedException {
        // uri = https://yfapi.net/v6/finance/quote?region=US&lang=en&symbols=AAPL
        String response = fetchData(uri, apiKey);
        JSONObject responseJsonObj = JSONObject.parseObject(response);
        return responseJsonObj
                .getJSONObject("quoteResponse")
                .getJSONArray("result")
                .getJSONObject(0)
                .getDouble("bid");
    }

    public static double fetchLiveSell(String uri, String apiKey) throws IOException,
            InterruptedException {
        String response = fetchData(uri, apiKey);
        JSONObject responseJsonObj = JSONObject.parseObject(response);
        return responseJsonObj
                .getJSONObject("quoteResponse")
                .getJSONArray("result")
                .getJSONObject(0)
                .getDouble("ask");
    }

    public static void main(String[] args) throws IOException, InterruptedException {
//        String uri = "https://yfapi.net/v8/finance/chart/AAPL";
        String uri = "https://yfapi.net/v6/finance/quote?region=US&lang=en&symbols=AAPL";
        String apiKey = "0Mq7QXQbm4310EhSxPCx6o9w5Jp1yT643uO0YCn9";
        double bid = fetchLiveBuy(uri, apiKey);
        double ask = fetchLiveSell(uri, apiKey);
        System.out.println(bid);
        System.out.println(ask);
    }
}
